# condenast

- **Task description** - https://github.com/conde-nast-international/cnid-tech-tests/tree/master/infra-task

**Pre-requiste: -**
- One should have terraform installed. ( Above code is tested on Terraform v0.14.6) 
- Need to setup aws credentials
- Need to create ssh key pair (required to connect to ec2 instance)
- Need to replace IP address (103.211.58.59) with <yours IP address>(http://ipv4.icanhazip.com) in **variable.tf** under **iac** directory.

**Approch -**
 - Mostly fosuced on provisioning infra as a code. This will provision 24 resources & one can access the application using the output url.
 - Used docker container to run the nodejs application.

**Terraform code: -**
- Above code is fully modulized (tf-modules).
- One should need to replace ssh private_key(required to run remote) & public_key in module key-pair & ec2 respectively.

```
module "key-pair" {
  source   = "../"
  key-path = file("<public_key>")
}
```


```
module "ec2" {
  source        = "../"
  private_key = "<private_key>"
}
```


**Single click Solution:-**
 - Above code uses docker-compose in order to serve application (One can have better solution like running ECS).

**Commands-**
  - terraform init
  - terraform plan
  - terraform apply
 
 terraform result with **output url**: "<dns>" (Please copy this into your browser and hit :))
