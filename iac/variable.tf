variable "region" {
  description = "AWS Deployment region.."
  default     = "us-east-1"
}

variable "env" {
  default = "dev"
}

variable "cidr_block" {
  default = "10.0.0.0/16"
}

variable "public_subnets_cidr" {
  type    = list(any)
  default = ["10.0.1.0/24", "10.0.2.0/24"]
}

variable "private_subnets_cidr" {
  type    = list(any)
  default = ["10.0.3.0/24", "10.0.4.0/24"]
}

variable "availability_zones" {
  type    = list(any)
  default = ["us-east-1a", "us-east-1b"]
}

variable "public-subnet" {
  default = "public"
}

variable "private-subnet" {
  default = "private"
}


variable "sg_ingress_rules" {
  type = list(object({
    from_port   = number
    to_port     = number
    protocol    = string
    cidr_block  = string
    description = string
  }))

  default = [
    {
      from_port   = 22
      to_port     = 22
      protocol    = "tcp"
      cidr_block  = "103.211.58.59/32"
      description = "test"
    },
    {
      from_port   = 8080
      to_port     = 8080
      protocol    = "tcp"
      cidr_block  = "103.211.58.59/32"
      description = "web"
    }

  ]
}

variable "sg_egress_rules" {
  type = list(object({
    from_port   = number
    to_port     = number
    protocol    = string
    cidr_block  = string
    description = string
  }))
  default = [
    {
      from_port   = 0
      to_port     = 0
      protocol    = "-1"
      cidr_block  = "0.0.0.0/0"
      description = "test"
    }
  ]
}

variable "instance_type" {
  default = "t2.micro"
}

###ALB###
variable "sg_lb_egress_rules" {
  type = list(object({
    from_port   = number
    to_port     = number
    protocol    = string
    cidr_block  = string
    description = string
  }))
  default = [
    {
      from_port   = 0
      to_port     = 0
      protocol    = "-1"
      cidr_block  = "0.0.0.0/0"
      description = "test"
    }
  ]
}
variable "sg_lb_ingress_rules" {
  type = list(object({
    from_port   = number
    to_port     = number
    protocol    = string
    cidr_block  = string
    description = string
  }))
  default = [
    {
      from_port   = 80
      to_port     = 80
      protocol    = "tcp"
      cidr_block  = "103.211.58.59/32"
      description = "web"
    }
  ]
}

variable "alb-listener-port" {
  default = 80
}

variable "alb-listener-protocol" {
  default = "HTTP"
}

variable "target-group-path" {
  default = "/"
}
variable "target-group-port" {
  default = 8080
}
variable "alb-svc-port" {
  default = 8080
}

variable "alb-path" {
  default = "/"
}
