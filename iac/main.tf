module "vpc" {
  source      = "../tf-modules/modules/networking/vpc"
  environment = var.env
  vpc_cidr    = var.cidr_block
}

module "igw" {
  source      = "../tf-modules/modules/networking/igw"
  vpc_id      = module.vpc.vpc_id
  environment = var.env

}

module "public-subnet" {
  source             = "../tf-modules/modules/networking/subnet"
  vpc-id             = module.vpc.vpc_id
  environment        = var.env
  subnets_cidr       = var.public_subnets_cidr
  availability_zones = var.availability_zones
  subnet-type        = var.public-subnet
}


module "public-rt" {
  source      = "../tf-modules/modules/networking/route-table"
  vpc-id      = module.vpc.vpc_id
  environment = var.env
  subnet-type = var.public-subnet
}

module "public-route" {
  source      = "../tf-modules/modules/networking/routes"
  rt-id       = module.public-rt.rt_id
  gw-id       = module.igw.igw_id
  subnet-type = var.public-subnet
  environment = var.env

}

module "rt-ass-public" {
  source      = "../tf-modules/modules/networking/route-associate"
  subnet-cidr = var.public_subnets_cidr
  subnet-id   = module.public-subnet.sub_id
  rt-id       = module.public-rt.rt_id
}

module "security-group" {
  source            = "../tf-modules/modules/networking/security-group"
  environment       = var.env
  vpc-id            = module.vpc.vpc_id
  resource-attached = "instance"
}

module "sg-ingress-rule" {
  source    = "../tf-modules/modules/networking/security-group-rules"
  rule-type = "ingress"
  rules     = var.sg_ingress_rules
  sg-id     = module.security-group.sg_id
}

module "sg-sg" {
  source          = "../tf-modules/modules/networking/sg_to_sg_rule"
  rule-type       = "ingress"
  from_port       = 8080
  to_port         = 8080
  protocol        = "TCP"
  security_groups = module.lb-security-group.sg_id
  description     = "ALB SG to EC2"
  sg-id           = module.security-group.sg_id
}

module "sg-egress-rule" {
  source    = "../tf-modules/modules/networking/security-group-rules"
  rule-type = "egress"
  rules     = var.sg_egress_rules
  sg-id     = module.security-group.sg_id
}

module "key-pair" {
  source   = "../tf-modules/modules/compute/key-pair"
  key-path = file("../../.ssh/id_rsa.pub")
}

module "ec2" {
  source        = "../tf-modules/modules/compute/ec2-wud"
  instance-type = var.instance_type
  subnet-id     = module.public-subnet.sub_id[0]
  sg-id         = module.security-group.sg_id
  environment   = var.env
  key-name      = module.key-pair.key_name
  // src         = file("../docker/docker-compose.yaml")
  // dest        = "/tmp"
  resource-attached = "web"
  private_key = "../../.ssh/id_rsa"
}

#ALB 

module "lb-security-group" {
  source            = "../tf-modules/modules/networking/security-group"
  environment       = var.env
  vpc-id            = module.vpc.vpc_id
  resource-attached = "lb"
}

module "sg-lb-ingress-rule" {
  source    = "../tf-modules/modules/networking/security-group-rules"
  rule-type = "ingress"
  rules     = var.sg_lb_ingress_rules
  sg-id     = module.lb-security-group.sg_id
}

module "sg-lb-egress-rule" {
  source    = "../tf-modules/modules/networking/security-group-rules"
  rule-type = "egress"
  rules     = var.sg_lb_egress_rules
  sg-id     = module.lb-security-group.sg_id
}

module "alb" {
  source                = "../tf-modules/modules/compute/lb/alb-res/alb"
  subnet-id             = module.public-subnet.sub_id
  sg-id                 = module.lb-security-group.sg_id
  alb_listener_port     = var.alb-listener-port
  alb_listener_protocol = var.alb-listener-protocol
  target_group_arn      = module.alb-tg.alb_tg_arn
  alb_path              = var.alb-path
  alb-tg-id             = module.alb-tg.alb_tg_arn
  environment           = var.env
}

module "alb-tg" {
  source            = "../tf-modules/modules/compute/lb/alb-res/lb-target-group"
  vpc-id            = module.vpc.vpc_id
  alb_svc_port      = var.alb-svc-port
  target_group_path = var.target-group-path
  target_group_port = var.target-group-port
  environment       = var.env
}

module "alb-tg-attachment" {
  source          = "../tf-modules/modules/compute/lb/alb-res/lb-target-group-attachment"
  aws-instance-id = module.ec2.aws_instance_id
  alb_tg_arn      = module.alb-tg.alb_tg_arn
}

// module "create_ami" {
//   source          = "../tf-modules/modules/compute/ami"
//   environment     = var.env
//   ec2_instance_id = module.ec2.aws_instance_id

// }

// module "autoscaling" {
//   source        = "../tf-modules/modules/compute/asg"
//   name          = "${var.env}-autosacling"
//   ami_id        = module.create_ami.new-ami-id
//   instance-type = var.instance_type
//   environment   = var.env
//   tg-arn        = module.alb-tg.alb_tg_arn
//   subnet-id = flatten(module.public-subnet.*.sub_id)[0]
//   sg = module.security-group.sg_id
// }
