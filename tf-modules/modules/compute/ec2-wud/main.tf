data "aws_ami" "ubuntu" {
  most_recent = true

  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-focal-20.04-amd64-server-*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  owners = ["099720109477"] # Canonical
}
#
resource "null_resource" "remote_provisioner" {
  triggers = {
    public_ip = aws_instance.ec2-instance.public_ip
  }

  connection {
    type  = "ssh"
    host  = aws_instance.ec2-instance.public_ip
    user  = "ubuntu"
    port  = 22
    private_key = file(var.private_key)
  }

  // copy our example script to the server
  provisioner "file" {
    source      = "${path.module}/docker-compose.yaml"
    destination = "/tmp/docker-compose.yaml"
  }

  provisioner "remote-exec" {
    inline = [
          "sudo apt update -y",
          "sudo apt install apt-transport-https ca-certificates curl software-properties-common -y",
          "curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -",
          "sudo add-apt-repository 'deb [arch=amd64] https://download.docker.com/linux/ubuntu focal stable'",
          "sudo apt update -y",
          "apt-cache policy docker-ce",
          "sudo apt install docker-ce -y",
          "sudo curl -L \"https://github.com/docker/compose/releases/download/1.29.1/docker-compose-$(uname -s)-$(uname -m)\" -o /usr/local/bin/docker-compose",
          "sudo chmod +x /usr/local/bin/docker-compose",
          "sudo ln -s /usr/local/bin/docker-compose /usr/bin/docker-compose",
          "cd /tmp/",
          "sudo docker-compose up -d"
    ]
  }
}

resource "aws_instance" "ec2-instance" {
  ami                         = data.aws_ami.ubuntu.id
  instance_type               = var.instance-type
  subnet_id                   = var.subnet-id
  vpc_security_group_ids      = [var.sg-id]
  key_name                    = var.key-name
  associate_public_ip_address = true

  tags  = {
    Name = "${var.environment}-${var.resource-attached}-instacne"
  }
}

