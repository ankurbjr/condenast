resource "aws_launch_template" "temp_auto_scale" {
  name_prefix   = "${var.environment}-autosacling_template"
  image_id      = var.ami_id
  instance_type = var.instance-type
  vpc_security_group_ids = [var.sg]

}

resource "aws_autoscaling_group" "asg" {
  desired_capacity   = 1
  max_size           = 1
  min_size           = 1
  target_group_arns  = [var.tg-arn]
  vpc_zone_identifier = [ var.subnet-id ]

  launch_template {
    id      = aws_launch_template.temp_auto_scale.id
    version = "$Latest"
  } 
}