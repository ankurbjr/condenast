variable "name" {}
variable "environment" {}
variable "ami_id" {}
variable "instance-type" {}
variable "tg-arn" {}
variable "subnet-id" {}
variable "sg" {}