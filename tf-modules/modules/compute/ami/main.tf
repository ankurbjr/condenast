resource "aws_ami_from_instance" "new_ami" {
  name               = "${var.environment}-new-ami"
  source_instance_id = var.ec2_instance_id
}
